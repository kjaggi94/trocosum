const menu = document.getElementById('menu');
const nav_btn = document.getElementById('nav-tgl');

nav_btn.addEventListener('click', evt => {
    if (menu.className.indexOf('active') === -1) {
        menu.classList.add('active');
    } else {
        menu.classList.remove('active');
    }
})